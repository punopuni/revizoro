var gulp 		      = require('gulp'),
	  stylus 		    = require('gulp-stylus'),
    jade          = require('gulp-jade'),
  	plumber 	    = require('gulp-plumber'),
  	browserSync   = require('browser-sync'),
  	del           = require('del'),
  	concat        = require('gulp-concat'),
  	imagemin      = require('gulp-imagemin'),
  	// sass          = require('gulp-sass'),
  	sourcemaps    = require('gulp-sourcemaps'),
  	uglify        = require('gulp-uglify'),
  	autoprefixer  = require('gulp-autoprefixer'),
  	cache         = require('gulp-cache'),
  	pngquant      = require('imagemin-pngquant'),
    gutil         = require('gulp-util'),
    notify        = require('gulp-notify'),
	  beep          = require('beepbeep');

// destination folder
var dest='static/dist/';

// error handler function
var onError = function (error) {

  notify({
    title: 'Task Failed [' + error.plugin + ']',
    message:  error.toString(),
  }).write(error);
  console.error(error.toString());

  this.emit('end');
};

var src_paths = {
		stylus: 'static/app/stylus/**/[^_]*.styl',
		// sass:   'static/app/sass/**/*+(scss|sass)',
		scripts: 'static/app/js/**/*.js',
		images: 'static/app/img/**/*',
    jade: 'static/app/jade/**/[^_]*.jade',
		fonts: 'static/app/fonts/**/*',
		vendor: 'static/app/vendor/**/*'
};

var watch_paths = {
		stylus: 'static/app/stylus/**/*.styl',
		// sass:   'static/app/sass/**/*+(scss|sass)',
    jade: 'static/app/jade/**/*.jade',
		scripts: 'static/app/js/**/*.js',		
		images: 'static/app/img/**/*',
		fonts: 'static/app/fonts/**/*',
		vendor: 'static/app/vendor/**/*'
};

gulp.task('stylus', function() {
	  return gulp.src(src_paths.stylus) 				// get source paths from array above
    .pipe(sourcemaps.init())                                // helps connect source files and production files
		.pipe(plumber({errorHandler: onError})) // prevents gulp.watch from crashing, finds errors in stream
		.pipe(stylus())
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы 												// compiles stylus
		.pipe(sourcemaps.write()) 							// helps connect source files and production files
		.pipe(gulp.dest(dest + 'css')) 					// send result to css folder in build
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('jade', function() {
    return gulp.src(src_paths.jade)
    .pipe(sourcemaps.init())
    .pipe(plumber({errorHandler: onError})) // plumber finds errors in stream
    .pipe(jade({pretty: true,}))
    .pipe(sourcemaps.write()) 
    .pipe(gulp.dest(dest))
});

gulp.task('browser-sync', function() {
  browserSync.init({
    startPath:'static/dist',
    server: {
      baseDir: './',
    },
    notify: false
  });
});

gulp.task('watch', ['stylus','jade',
					'scripts','img','fonts','vendor','browser-sync'], function() {
    gulp.watch(watch_paths.stylus, ['stylus']);
    gulp.watch(watch_paths.jade, ['jade', browserSync.reload]);
    gulp.watch(watch_paths.images, ['img']);
    gulp.watch(watch_paths.fonts, ['fonts']);
    gulp.watch(watch_paths.vendor, ['vendor']);
    gulp.watch(watch_paths.scripts, ['scripts']);
});

gulp.task('scripts', function() {
  return gulp.src(src_paths.scripts)
    .pipe(sourcemaps.init())
    .pipe(plumber({errorHandler: onError})) // plumber finds errors in stream
    .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dest + 'js'))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('img', function() {
    return gulp.src(src_paths.images) // Берем все изображения из app
        .pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
            interlaced: true,
            progressive: true,
            optimizationLevel: 5,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest(dest + 'img')); // Выгружаем на продакшен
});

gulp.task('fonts', function() {
  return gulp.src(src_paths.fonts)
    .pipe(gulp.dest(dest + 'fonts'));
});

gulp.task('vendor', function() {
  return gulp.src(src_paths.vendor)
    .pipe(gulp.dest(dest + 'vendor'));
});

gulp.task('clean', function () {
    return del.sync(dest);
});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('default', ['watch']);


// gulp.task('sass', function() {
// 	return gulp.src(src_paths.sass) 				
// 		.pipe(plumber({errorHandler: onError}))
// 		.pipe(sourcemaps.init()) 								
// 		.pipe(sass()) 											
// 		.pipe(sourcemaps.write()) 							
// 		.pipe(gulp.dest(app + 'css')) 					
// 		.pipe(browserSync.reload({stream: true}))
// });