<?php 
/**
* 
*/
class Slider extends BaseModel
{
	public static $tablename = 'slides';
	
	public static $fields = array(
		'id' 				=> 'integer',
		'idx'				=> 'integer',
		'active' 			=> 'bool',
		'image'				=> 'image',
	);

}