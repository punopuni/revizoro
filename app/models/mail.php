<?php 
/**
* 
*/
class Mail
{
	public static function send($message, $subject, $to)
	{
		if (! $to) return false;

		if (gettype($to) == 'string') {
			$to = explode(',', $to);
		}

		foreach ($to as $mail) {
			self::sendMessage($message, $subject, $mail);
		}
	}

	private static function sendMessage($message, $subject, $mail)
	{
		$email = new PHPMailer();

        if (getenv('ENV') == 'docker_dev') {
            $email->isSMTP();                      // Set mailer to use SMTP
            $email->Host     = 'mailcatcher:1025'; // Specify main and backup SMTP servers
            $email->Port     = 1025;               // TCP port to connect to
        }

		$email->setLanguage('ru', $_SERVER['DOCUMENT_ROOT'].'/vendor/PHPMailer/language/directory/');
		$email->setFrom('robot@akrevizor.kz', 'akrevizor.kz');
		$email->Subject   = $subject;
		$email->Body      = $message;
		$email->CharSet   = 'UTF-8';
		$email->IsHTML(true);
		$email->AddAddress($mail);

		return $email->send();
	}
}