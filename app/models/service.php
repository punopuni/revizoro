<?php 
/**
* 
*/
class Service extends BaseModel
{
	
	public static $tablename = 'services';
	
	public static $fields = array(
		'id' 				=> 'integer',
		'active' 			=> 'bool',
		'description'		=> 'string',
		'icon'				=> 'image',
	);

}