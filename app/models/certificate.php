<?php
/**
*
*/
class Certificate extends BaseModel
{

	public static $tablename = 'certificates';

	public static $fields = array(
		'id' 				=> 'integer',
		'active' 			=> 'bool',
		'image'		        => 'image',
		'description'			    => 'text',
	);

	public static $requiered_fields = array(
		'image'		        => 'text',
		'description'			    => 'text',
	);

}