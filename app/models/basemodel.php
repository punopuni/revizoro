<?php

/**
 * Базовая модель
 */
class BaseModel
{


  public static $tablename = '';
  public static $mysqli = '';
  public static $fields = '';

  function __construct()
  {
    if (!static::$fields) {
      return;
    }

    foreach (array_keys(static::$fields) as $field) {
      $this->$field = false;
    }

    $this->active = 1;
  }

  public static function __connect()
  {
    if (getenv('ENV') == 'docker_dev') {
      $servername = "mysql55";
      $username = "root";
      $password = "";
      $dbname = "revizor-kz";
    } elseif ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {//Это OpenServer
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "revizor-kz";
    } else {
      $servername = "srv-pleskdb38.ps.kz";
      $username = "akrevizo_kz";
      $password = "V3ebuAxYQxK3J86E";
      $dbname = "akrevizo_kz";
//      $servername = "localhost";
//      $username = "id10567527_root";
//      $password = "root123";
//      $dbname = "id10567527_revizor";
    }

    setlocale(LC_ALL, "ru_RU.UTF-8");
    define('PREFIX', 'ru_');

    $mysqli = new mysqli($servername, $username, $password, $dbname) or die ("Ошибка подключения");
    $mysqli->query("SET NAMES utf8");
    $mysqli->query('SET character set utf8');

    self::$mysqli = $mysqli;
  }

  /**
   * Запрос на поиск данных в бд, если не указаны параметры, выведем 100 последних записей, сортированных по id
   */
  public static function select($where = '1 = 1', $limit = 100, $order_by = '`id`', $offset = 0)
  {
    $result = array();
    if (is_numeric($where)) {
      $where = 'id = ' . $where;
    } elseif (is_array($where)) {
      foreach ($where as $field => $type) {
        $string_where .= $field . ',';
      }
      $where = preg_replace('/(.+),$/', '\\1', $string_where);
    }

    // Таблицы должны быть с префиксом, в целях безопастности
    $tablename = PREFIX . static::$tablename;
    $query = ' SELECT * FROM ' . $tablename . ' WHERE '
      . $where . ' ORDER BY ' . $order_by . ' LIMIT ' . $limit;
    $query_result = static::$mysqli->query($query) or die(var_dump(static::$mysqli->error . '<sbr>' . $query));

    while ($row = mysqli_fetch_object($query_result)) {
      $class = get_called_class();
      $post = new $class;
      foreach ($row as $key => $value) {
        $post->$key = $value;
      }
      $result[$post->id] = $post;
    }

    return $result;
  }

  /**
   *  Выводим данные по дефолтными настройкам функции select
   */
  public static function selectAll()
  {
    return static::select();
  }

  /**
   *  Поиск одной записи
   */
  public static function selectOne($where = '1 = 1', $limit = 1, $order_by = 'id')
  {
    $data = static::select($where, $limit, $order_by);
    return current($data);
  }

  public function setAll($data)
  {
    foreach (static::$fields as $field => $type) {
      if (in_array($type, ['image',
        'file'])) {
        $data[$field] = $type;
      }
      if (isset($data[$field])) {
        $this->set($field, $data[$field]);
      }
    }
  }

  public function set($field, $value)
  {
    if (method_exists($this, 'set_' . $field)) {
      $this->$field = $this->{'set_' . $field}($value);
      return;
    }

    switch (static::$fields[$field]) {
      case 'text':
        $this->$field = addslashes($value);
        break;

      case 'array':
        $this->$field = json_encode($value);
        break;

      case 'image':
        $this->$field = $this->uploadImage($field);
        break;

      case 'file':
        $this->$field = $this->uploadFile($field);
        break;

      case 'datetime':
        if ((int)$value > 10000) {
          $this->$field = strtotime($value);
        } else {
          $this->$field = $value;
        }
        break;

      default:
        $this->$field = $value;
        break;
    }
  }

  public function save()
  {

    $insert_fields = '';
    $insert_values = '';

    foreach (static::$fields as $field => $type) {
      if ($this->$field) {
        $insert_fields .= '`' . $field . '`, ';
        $insert_values .= '\'' . $this->$field . '\', ';
      }
    }

    $insert_values = rtrim($insert_values, ', ');
    $insert_fields = rtrim($insert_fields, ', ');

    $tablename = PREFIX . static::$tablename;
    $query = ' INSERT INTO `' . $tablename . '` ( ' . $insert_fields . ' ) VALUES ( ' . $insert_values . ' );';
    $query_result = static::$mysqli->query($query);

    return $this;
  }

  public function get($field)
  {
    if (!static::$fields) return $this->$field;
    if (method_exists($this, 'get_' . $field)) {
      return $this->{'get_' . $field}();
    }
    switch (static::$fields[$field]) {
      case 'text':
        $result = stripslashes($this->$field);
        break;

      case 'array':
        $result = json_decode($this->$field);
        break;

      case 'string_array':
        $result = explode(PHP_EOL, $this->$field);
        break;

      case 'datetime':
        $result = date('d-m-Y H:i:s', $value);
        break;

      default:
        $result = $this->$field;
        break;
    }

    return $result;
  }

  public function uploadImage($field)
  {
    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/upload_images/";

    $target_file = $target_dir . basename($_FILES[$field]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $imageName = (substr(md5(uniqid()), 0, 32)) . '.' . $imageFileType;
    $target_file = $target_dir . $imageName;
    if (isset($_FILES[$field]) && $_FILES[$field]["tmp_name"]) {
      $check = getimagesize($_FILES[$field]["tmp_name"]);
      if ($check !== false) {
        $uploadOk = 0;
      } else {
        $uploadOk = 1;
      }
    } else {
      return;
    }
    if (file_exists($target_file)) {
      $uploadOk = 3;
    }
    if ($_FILES[$field]["size"] > 5000000) {
      $uploadOk = 4;
    }
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif") {
      $uploadOk = 5;
    }
    if ($uploadOk != 0) {
      var_dump($uploadOk);
    } else {
      if (move_uploaded_file($_FILES[$field]["tmp_name"], $target_file)) {
        return $imageName;
      } else {

        return 'file not uploaded';
      }
    }
  }

  /**
   * Метод загрузки файлов
   */
  public function uploadFile($field, $file)
  {
    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/upload_files/";

    $target_file = $target_dir . basename($_FILES[$field]["name"]);
    $uploadOk = 1;
    $fileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $fileName = (substr(md5(uniqid()), 0, 32)) . '.' . $fileType;
    $target_file = $target_dir . $fileName;

    if (isset($_FILES[$field])) {
      $check = getimagesize($_FILES[$field]["tmp_name"]);
      if ($check !== false) {
        $uploadOk = 0;
      } else {
        $uploadOk = 1;
      }
    }
    if (file_exists($target_file)) {
      $uploadOk = 2;
    }
    if ($_FILES[$field]["size"] > 500000) {
      $uploadOk = 3;
    }
    if ($fileType == "exe" || $fileType == "bat" || $fileType == "ch"
      || $fileType == "php") {
      $uploadOk = 4;
    }
    if ($uploadOk != 0) {
      var_dump($uploadOk);
    } else {
      if (move_uploaded_file($_FILES[$field]["tmp_name"], $target_file)) {
        return $fileName;
      } else {
        var_dump('file not upload');
      }
    }
  }

  /**
   * Изменение записи в бд
   */

  public function update()
  {
    $string_where = '';
    $set_string = '';

    if (!$this->id) {
      return 'empty request';
    }

    $this->last_changes = time();

    foreach (static::$fields as $field => $type) {
      if ($this->$field) {
        $set_string .= $field . ' = \'' . $this->$field . '\',';
      }
    }


    $tablename = PREFIX . static::$tablename;

    $set_string = preg_replace('/(.+),$/', '\\1', $set_string);

    $tablename = PREFIX . static::$tablename;
    $query = ' UPDATE `' . $tablename . '` SET ' . $set_string . ' WHERE id = ' . $this->id . ';';

    $query_result = static::$mysqli->query($query);

    return $this;
  }

  // DELETE FROM database
  public function drop($where)
  {
    if (is_numeric($where)) {
      $where = 'id = ' . $where;
    } elseif (is_array($where)) {
      foreach ($where as $field => $type) {
        $string_where .= $field . ',';
      }
      $where = preg_replace('/(.+),$/', '\\1', $string_where);
    }

    $entity = $this->select($where);
    if ((isset($entity['file']) && $entity['file']) || (isset($entity['image']) && $image['image'])) {
      # code...
    }

    $tablename = PREFIX . static::$tablename;
    $query = ' DELETE FROM `' . $tablename . '` WHERE ' . $where;

    $query_result = static::$mysqli->query($query);
  }

  public function load($id)
  {
    $entity = static::selectOne($id);

    foreach (static::$fields as $field => $type) {
      $this->$field = $entity->$field;
    }

    return $this;
  }
}
