<?php 
/**
* 
*/
class User extends BaseModel
{
	
	public static $tablename = 'users';
	
	public static $fields = array(
		'id' 				=> 'integer',
		'name'				=> 'string',
		'last_changes' 		=> 'datetime',
		'active' 			=> 'bool',
		'mail'				=> 'string',
		'login'				=> 'string',
		'surname'			=> 'string',
		'password'			=> 'password',
	);

	public static $requiered_fields = array(
		'login'				=> 'string',
		'password'			=> 'password',
	);

}