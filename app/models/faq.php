<?php
/**
*
*/
class Faq extends BaseModel
{

	public static $tablename = 'faqs';

	public static $fields = array(
		'id' 				  => 'integer',
		'active' 			=> 'bool',
		'question'		=> 'text',
		'answer'			=> 'text',
	);

	public static $requiered_fields = array(
		'question'		=> 'text',
		'answer'			=> 'text',
	);

}