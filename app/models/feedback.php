<?php 
/**
* 
*/
class Feedback extends BaseModel
{
	public static $tablename = 'feedback';
	
	public static $fields = array(
		'id' 				=> 'integer',
		'active'			=> 'bool',
		'name'				=> 'string',
		'phone'				=> 'string',
		'email'				=> 'string',
		'message'			=> 'text',
	);


	public static $required_fields = [
		'name'				=> 'string',
		'phone'				=> 'string',
		'message'			=> 'text',
	];

	public function validate()
	{
		$errors = [];
		foreach (static::$required_fields as $field => $type) {
			if ($field == 'phone') {
                $phone_only_numbers = preg_replace('/\D/', '', $this->get($field));
                if (strlen($phone_only_numbers) !== 11) {
                    $errors[$field] = 'Некорректный ввод номера';
                }
                continue;
			}
			if (! $this->get($field)) {
				$errors[$field] = 'Поле не должно быть пустым';
			}
		}
		return $errors;
	}
}