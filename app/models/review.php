<?php
/**
*
*/
class Review extends BaseModel
{

	public static $tablename = 'reviews';

	public static $fields = array(
		'id' 				=> 'integer',
		'active' 			=> 'bool',
		'image'		        => 'image',
		'description'			    => 'text',
	);

	public static $requiered_fields = array(
		'image'		        => 'text',
		'description'			    => 'text',
	);

}