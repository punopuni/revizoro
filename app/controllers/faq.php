<?php
/**
*
*/
class FaqController extends BaseController
{
	public $title = 'Вопрос-Ответ';

	public function indexCall()
	{
		$vars = array(
			'title' 		=> $this->title,
			'faqs'		  => Faq::selectAll(),
		);

//		die(var_dump($vars));

		return $this->include_to_template('faq', $vars);
	}

}