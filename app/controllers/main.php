<?php 
/**
* 
*/
class MainController extends BaseController
{
	public $title = 'Главная';

	public function indexCall()
	{
		$vars = array(
			'title' 		=> $this->title,
			'services'		=> Service::selectAll(),
		);

		return $this->include_to_template('main', $vars);
	}

}	