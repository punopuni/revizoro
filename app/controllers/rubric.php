<?php
/**
* 	
*/
class RubricController extends BaseController
{

	public function indexCall()
	{
		if (! isset($_GET['furl']) || ! $_GET['furl']) {
			do404();
		}
		$furl = $_GET['furl'];
		$page = Rubric::selectOne('furl = \''.$furl.'\'');

		if (! $page) {
			do404();
		}

		$vars = array(
			'title' 		=> $page->get('title'),
			'page'			=> $page,
		);

		return $this->include_to_template('page', $vars);
	}
}