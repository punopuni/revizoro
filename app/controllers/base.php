<?php 
/**
* Родитель всех классов
*/
class BaseController
{
	public static $edit_only = false;

	function __construct()
	{
		$this->before();
	}

	/**
	 *  Функиция выполняемая первой, перед другими
	 */
	public function before($vars = array())
	{
	}

	/**
	 * Отправляем данные в базовый шаблон
	 */
	public function include_to_template($template, $vars = array(), $base_template = 'template')
	{
		$content = include_file($template, $vars);

		// Еcли не объявлен title, берем текущий url
		$title = $_SERVER['REQUEST_URI'];

		// Дополнительные скрипты и стили для каждой страницы
		$extra_scripts = array();
		$extra_styles  = array();

		if (isset($vars['title'])) {
			$title = $vars['title'];
		}

		if (isset($this->extra_scripts)) {
			$extra_scripts = $this->extra_scripts;
		}

		if (isset($this->extra_styles)) {
			$extra_styles = $this->extra_styles;
		}

		$vars = array(
			'content' => $content,
			'title' => $title,
			'extra_scripts' => $extra_scripts,
			'extra_styles' => $extra_styles,
		);

		if ($base_template) {
			return include_file($base_template, $vars);
		} else {
			return $content;
		}
	}

	function __destruct()
	{
		$this->after();
	}

	/**
	 * Функция выполняемая после всех остальных
	 */
	public function after($vars = array())
	{
	}	
}