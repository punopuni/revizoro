<?php 
/**
* 
*/
class FeedbackController extends BaseController
{
	public function indexCall()
	{
		$entity = new Feedback;
		$entity->setAll($_POST);
		$errors = $entity->validate();

		if (!empty($errors)) {
			$vars = [
				'errors' => $errors,
				'entity' => $entity,
			];
			return json_encode(['html'=>include_file('feedback_form', $vars)]);
		}

		$entity->save();

		$message = 'Здравствуйте администратор. Пользователь ' . $entity->get('name') . ' отправил нам сообщение: "' . nl2br($entity->get('message') .'". Его номер телефона: ' . $entity->get('phone')) . ($entity->get('email')?', его email: '.$entity->get('email'):'') . '.';

		$subject = 'Новое обращение по обратной связи на сайте ' . $_SERVER['SERVER_NAME'];
		$to = $GLOBALS['config']['admin_mails']?:'eduardeliseev.m@gmail.com';

		Mail::send($message, $subject, $to);
		return json_encode(['html'=>include_file('success')]);
	}
}