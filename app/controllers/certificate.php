<?php

class CertificateController extends BaseController
{
	public $title = 'Отзывы';

	public function indexCall()
	{
		$vars = array(
			'title' 		=> $this->title,
			'certificates'		  => Certificate::selectAll(),
		);

//		die(var_dump($vars));

		return $this->include_to_template('certificate', $vars);
	}

}