<?php

class ReviewController extends BaseController
{
	public $title = 'Отзывы';

	public function indexCall()
	{
		$vars = array(
			'title' 		=> $this->title,
			'reviews'		  => Review::selectAll(),
		);

//		die(var_dump($vars));

		return $this->include_to_template('review', $vars);
	}

}