<div class="form-group">
	<div class="col-xs-12">
		<?php $field = 'name' ?>
		<label for="<?= $field ?>" class="control-label">Ваше Имя</label>
		<input type="text" class="form-control" id="<?= $field ?>" value="<?= isset($entity->$field) && $entity->get($field)?$entity->get($field):'' ?>" name="<?= $field ?>" required="required">
		<?php if (isset($errors[$field]) && $errors[$field]): ?>
			<small class="error"><?= $errors[$field]?></small>
		<?php endif ?>
	</div>
</div>
<div class="form-group">
	<div class="col-xs-12">
		<?php $field = 'phone' ?>
		<label for="<?= $field ?>" class="control-label">Ваш телефон</label>
		<input type="phone" class="form-control" id="<?= $field ?>" value="<?= isset($entity->$field) && $entity->get($field)?$entity->get($field):'' ?>" name="<?= $field ?>" required="required">
		<?php if (isset($errors[$field]) && $errors[$field]): ?>
			<small class="error"><?= $errors[$field]?></small>
		<?php endif ?>
	</div>
</div>
<div class="form-group">
	<div class="col-xs-12">
		<?php $field = 'email' ?>
		<label for="<?= $field ?>" class="control-label">Ваш email</label>
		<input type="email" class="form-control" id="<?= $field ?>" value="<?= isset($entity->$field) && $entity->get($field)?$entity->get($field):'' ?>" name="<?= $field ?>">
		<?php if (isset($errors[$field]) && $errors[$field]): ?>
			<small class="error"><?= $errors[$field]?></small>
		<?php endif ?>
	</div>
</div>
<div class="form-group">
	<div class="col-xs-12">
		<?php $field = 'message' ?>
		<label for="<?= $field ?>" class="control-label">Сообщение</label>
		<textarea name="<?= $field ?>" class="form-control" id="<?= $field ?>" cols="30" rows="4" requried><?= isset($entity->$field) && $entity->get($field)?$entity->get($field):'' ?></textarea>
		<?php if (isset($errors[$field]) && $errors[$field]): ?>
			<small class="error"><?= $errors[$field]?></small>
		<?php endif ?>
	</div>
</div>
<div class="form-group">
	<div class="col-xs-12 text-center">
		<button type="submit" class="btn btn-primary">Отправить</button>
	</div>
</div>
