<div class="container">
    <h3 style="text-align: center; margin-top: 40px;">Отзывы</h3>
    <div class="gallery__wrapper">
        <?php foreach ($certificates as $certificate): ?>
            <div class="gallery_items">
                <a data-fancybox="gallery" data-caption="<?= $certificate->get('description') ?>"
                   href="/upload_images/<?= $certificate->get('image') ?>" class="gallery_item">
                    <img src="/upload_images/<?= $certificate->get('image') ?>"/>
                    <div class="gallery_item--description">
                        <?= $certificate->get('description') ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>