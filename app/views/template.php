<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width" id="viewport-content"/>
  <title><?= $title ?><?= isset($GLOBALS['meta_title']) ? ' ' . $GLOBALS['meta_title'] : '' ?>
    - <?= $_SERVER['SERVER_NAME'] ?></title>
  <?php if (isset($GLOBALS['config']['meta_descrition']) && $GLOBALS['config']['meta_description']): ?>
    <meta name="description" content="<?= $GLOBALS['config']['meta_description'] ?>">
  <?php endif; ?>
  <?php if (isset($GLOBALS['config']['meta_keywords']) && $GLOBALS['config']['meta_keywords']): ?>
    <meta name="keywords" content="<?= $GLOBALS['config']['meta_keywords'] ?>">
  <?php endif; ?>
  <meta name="author" content="Ed Eliseev, Igor Bytsko">
  <link rel="stylesheet" href="/static/dist/vendor/bootstrap/bootstrap.min.css"/>
  <link rel="stylesheet" href="/static/dist/vendor/bootstrap-select/bootstrap-select.min.css"/>
  <link rel="stylesheet" href="//use.fontawesome.com/fc28cc8f34.css"/>
  <link rel="stylesheet" href="/static/dist/vendor/jquery/dropzone/dropzone.css"/>
  <!-- slick slider-->
  <link rel="stylesheet" href="/static/dist/vendor/slick/slick.css"/>
  <link rel="stylesheet" href="/static/dist/vendor/jquery/jquery-ui.min.css"/>
  <link rel="stylesheet" href="/static/dist/vendor/slick/slick-theme.css"/>
  <!-- fancybox-->
  <link rel="stylesheet" href="/static/dist/vendor/fancybox/jquery.fancybox.min.css"/>
  <link rel="stylesheet" href="/static/dist/css/style.css?v=1"/>
</head>
<body>
<div class="wrapper">
  <header>
    <!--      <div class="line-left"></div>-->
    <!--      <div class="line-right"></div>-->
    <div class="container">
      <!--        <div class="hidden-xs hidden-sm header-line"></div>-->
      <div class="row display-f--header">
        <div class="col-md-4 col-xs-12 text-center"><a href="/"><img src="/static/dist/img/logo.png" alt="logo"
                                                                     class="logo img-responsive"/></a></div>
        <div class="col-md-8 col-xs-12">
          <?php $menu = Rubric::selectAll() ?>
          <ul class="menu">
            <li><a href="/">Главная</a></li>
            <?php foreach ($menu as $item): ?>
              <li><a href="/page/<?= $item->get('furl') ?>"><?= $item->get('title') ?></a></li>
            <?php endforeach ?>
            <li><a href="/certificate">Отзывы</a></li>
            <li><a href="/review">Наши клиенты</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>
  <main class="main_content">
    <div class="slider-wrapper">
      <div class="container slider">
        <?php $slides = Slider::select('1=1', 100, 'idx ASC'); ?>
        <?php if (!empty($slides)): ?>
          <?php foreach ($slides as $slide): ?>
            <div class="col-xs-12 slider-item"><img class="img-responsive"
                                                    src="/upload_images/<?= $slide->get('image') ?>"
                                                    alt="Слайд #<?= $slide->get('id') ?>"/></div>
          <?php endforeach ?>
        <?php endif ?>
      </div>
    </div>
    <?= $content ?>
  </main>
    <?= include_file('feedback_modal', $vars); ?>
  <footer style="flex: 0 0 auto;">
    <?php if (isset($GLOBALS['config']['license']) && $GLOBALS['config']['license']): ?>
      <div class="license-line">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 display-f" style="align-items: flex-start;">
              <i class="fa fa-file-text"> </i>
              <p><?= $GLOBALS['config']['license'] ?></p>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <div class="container display-f-footer" style="padding: 40px 0;">
      <div class="col-xs-12 col-sm-6 footer-left">
        <p>© <?= date('Y') ?> АК "Ревизоръ"</p>
      </div>

      <?php if (isset($GLOBALS['config']['footer_address']) && $GLOBALS['config']['footer_address']): ?>
        <div class="col-xs-12 col-sm-6 footer-right">
          <?= $GLOBALS['config']['footer_address'] ?>
        </div>
      <?php endif; ?>
    </div>
  </footer>
</div>
<script src="/static/dist/vendor/jquery/jquery.min.js"></script>
<script src="/static/dist/vendor/bootstrap/bootstrap.min.js"></script>
<script src="/static/dist/vendor/fancybox/jquery.fancybox.min.js"></script>
<!-- moment js-->
<script src="/static/dist/vendor/bootstrap-select/bootstrap-select.min.js"></script>
<!-- slick slider plugin-->
<script src="/static/dist/vendor/slick/slick.min.js"></script>
<script src="/static/dist/js/all.min.js"></script>
<!--<script src="//code.jivosite.com/widget.js" data-jv-id="6UdSXhrIbx" async></script><!--/U1BFOOTER1Z-->-->
</body>
</html>