<div class="container">
    <h3 style="text-align: center; margin-top: 40px;">Наши клиенты</h3>
    <div class="gallery__wrapper">
        <?php foreach ($reviews as $review): ?>
            <div class="gallery_items">
                <a data-fancybox="gallery" data-caption="<?= $review->get('description') ?>"
                   href="/upload_images/<?= $review->get('image') ?>" class="gallery_item">
                    <img src="/upload_images/<?= $review->get('image') ?>"/>
                    <div class="gallery_item--description">
                        <?= $review->get('description') ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>