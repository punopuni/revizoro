<div class="form__wrapper" style="background-color: #f4f3f4;">
  <div class="container" >
    <a class="btn btn-info feedback-btn" data-toggle="modal" href='#feedback'>Написать нам</a>
  </div>
</div>
<div class="modal fade" id="feedback">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="h4 text-center">Написать нам</div>
			</div>
			<div class="modal-body">
				<form action="/feedback" method="POST" class="form-horizontal ajax-form" role="form">
					<?= include_file('feedback_form', $vars) ?>
				</form>
			</div>
		</div>
	</div>
</div>