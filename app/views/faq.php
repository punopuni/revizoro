<div class="container">

    <?php if (isset($faqs) && $faqs): ?>
        <div class="panel-group" id="accordion">
            <!--  --><?php //die(var_dump($faqs)); ?>
            <?php foreach ($faqs as $faq): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion"
                               href="#collapse<?= $faq->get('id') ?>"><?= $faq->get('question') ?></a>
                        </h4>
                    </div>
                    <div id="collapse<?= $faq->get('id') ?>" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel-group">
                                <?= $faq->get('answer') ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
    <a class="btn btn-info feedback-btn" data-toggle="modal" href='#feedback'>Написать нам</a>
</div>
<div class="modal fade" id="feedback">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="h4 text-center">Написать нам</div>
            </div>
            <div class="modal-body">
                <form action="/feedback" method="POST" class="form-horizontal ajax-form" role="form">
                    <?= include_file('feedback_form', $vars) ?>
                </form>
            </div>
        </div>
    </div>
</div>