<div class="services-wrapper">
	<?php if ($services): ?>
		<div class="container services">
			<h1 class="h1">Наши услуги</h1>
			<?php $chunks = array_chunk($services, 4) ?>
			<?php foreach ($chunks as $services): ?>
				<div class="row">
				<?php foreach ($services as $service): ?>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<div class="service-item">
							<div class="service-item-icon">
								<img src="/upload_images/<?= $service->get('icon') ?>" alt=""/>
							</div>
							<div class="text text-center">
								<?= $service->get('description') ?>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				</div>
			<?php endforeach ?>
		</div>
	<?php endif ?>
</div>