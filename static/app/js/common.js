var sliders,
sliderinit = function(block) {
    var sliderConfig = {
        autoplay:true,
        autoplaySpeed:5500,
        arrows: false,
        slidesToShow: 1,
    };
    $(block).not('.slick-initialized').slick(sliderConfig);
}

// Начало основного кода
$(function(){
    $('#contacts_modal').on('show', function(){  
      initTooltip();
    });
    // $('#contacts-modal').modal('show');
    $('.slider').each(function() {
        console.log(this);
        sliderinit(this);
    });
});
