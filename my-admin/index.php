<?php 

define("ADMIN_SITE_WORK_DIR", $_SERVER['DOCUMENT_ROOT'].'/my-admin/app/controllers/', true);
define("ADMIN_SITE_SHOW_DIR", $_SERVER['DOCUMENT_ROOT'].'/my-admin/app/views/', true);
define("PREFIX", 'fs_', true);

date_default_timezone_set('Asia/Almaty');

if (getenv('ENV') == 'docker_dev') {
	error_reporting(E_ALL);
} else {
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
}

session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
spl_autoload_register(function ($class) {
	if (strpos($class, 'Controller') !== FALSE) {
		$class = str_replace('Controller', '', $class);
		$class = strtolower($class);

		$file = $_SERVER['DOCUMENT_ROOT'].'/my-admin/app/controllers/' . $class . '.php';
		if (file_exists($file)) {
			require_once $file;
		}
	} elseif (file_exists($_SERVER['DOCUMENT_ROOT'].'/app/models/'.strtolower($class).'.php')) {
		require_once $_SERVER['DOCUMENT_ROOT'].'/app/models/' . strtolower($class) . '.php';
	}
});

BaseModel::__connect();

$controller = isset($_GET['controller']) && $_GET['controller'] ? $_GET['controller'] : 'main';
$method = isset($_GET['method']) && $_GET['method'] ? $_GET['method'] : 'index';
$id     = isset($_GET['id']) && $_GET['id'] ? $_GET['id'] : '';

$file = ADMIN_SITE_WORK_DIR . $controller . '.php';


function include_file($file, $vars = array())
{
	
	foreach ($vars as $name => $var) {
		$$name = $var;
	}

	ob_start();

	$file = ADMIN_SITE_SHOW_DIR . $file . '.php';
	$file_fir = dirname($file);

	if (file_exists($file)) {
		require $file;
	}

	$return = ob_get_clean();

	return $return;

}

$controller = $controller . 'Controller';
$method = $method . 'Call';
if (! method_exists($controller, $method)) die('502');

$controller = new $controller;

echo $controller->{$method}($id);