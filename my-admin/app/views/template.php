<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="/my-admin/static/build/vendor/bootstrap.min.css">
	<link rel="stylesheet" href="/my-admin/static/build/css/style.css">
	<script src="/static/dist/vendor/jquery/jquery.min.js"></script>
	<title><?= $title ?></title>
	<meta charset="UTF-8">
</head>
<body>
	<div class="head container-fluid zero">
		<div class="logo col-xs-3">
			<img src="/my-admin/static/build/img/logo.png" alt="">
		</div>
		<div class="h-adress text-right col-xs-8 text-center ">
			<div class="h2">Панель управления сайтом <a href="/" target="_Blank"><?= $_SERVER['SERVER_NAME']?></a></div>
		</div>
		<div class="logout">
			<form method="POST" action="/my-admin/auth/logout">
				<button type="Submit"  class="btn-danger btn">Выйти</button>
			</form>
		</div>
	</div>
	<div class="container-fluid zero">
		<div class="left-panel col-xs-2">
			<div class="top zero">
				<img src="/my-admin/static/build/img/eagle-1.png" class="lpt-eagle img-responsive" alt="">
				<div class="menu">
					<div class="h4">Разделы</div>
					<ul>
						<li>
							<a href="/my-admin/user">Администраторы</a>
						</li>
						<li>
							<a href="/my-admin/rubric">Страницы сайта</a>
						</li>
						<li>
							<a href="/my-admin/service">Сервисы на главной</a>
						</li>
						<li>
							<a href="/my-admin/slider">Слайдер</a>
						</li>
						<li>
							<a href="/my-admin/feedback">Обратная связь</a>
						</li>
                        <li>
							<a href="/my-admin/certificate">Отзывы</a>
						</li>
                         <li>
							<a href="/my-admin/review">Наши клиенты</a>
						</li>
						<li>
							<a href="/my-admin/config">Настройки</a>
						</li>
					</ul>
				</div>
			</div>	
			<div class="bottom zero">
				<img src="/my-admin/static/build/img/lp-1.png" class="lpb-line img-responsive" alt="">
				<div class="menu">
				</div>
			</div>
			<div class="time zero">
				<img src="/my-admin/static/build/img/eagle-2.png" class="lpti-eagle img-responsive" alt="">
				<div id="time" class="text-center"></div>
			</div>
		</div>
		<div class="center col-xs-10 ">
			<img src="/my-admin/static/build/img/top-line.png" class="line-top zero" alt="">
			<img src="/my-admin/static/build/img/left-line.png" class="line-left zero" alt="">
			<div class="content">
			<div class="h2"><?= $vars['title'] ?></div>
				<?= $content ?>
			</div>
			<div class="w-vision text-right">
			<p> My-Vision CMS 1.1<br>
			Created by <a href="http://web-vision.kz">web-vision</a> team </p>
			</div>
		</div>
	</div>
	<script src="/my-admin/static/build/js/all.min.js"></script>
	<script src="/static/dist/vendor/ckeditor/ckeditor.js"></script>
	<script src="/static/dist/vendor/ckeditor/adapters/jquery.js"></script>
</body>
</html>	