<?php
 	$action = $_GET['method'] == 'change' ? '/my-admin/' . $_GET['controller'] . '/update/' . $_GET['id'] : '/my-admin/' . $_GET['controller'] . '/save';
?>
<form action="<?= $action ?>" method="POST" enctype="multipart/form-data">
	<?php foreach ($fields as $field => $type): ?>
		<?php if (($type == 'string') || ($type == 'integer')): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><input type="text" class="form-control" name="<?= $field ?>" value="<?= isset($values->$field) ? $values->get($field) : '' ?>"></p>
		<?php endif ?>
		<?php if ($type == 'password'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><input type="text" class="form-control" name="<?= $field ?>" value="" placeholder="Введите новый пароль"></p>
		<?php endif ?>
		<?php if ($type == 'datatime'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><input type="datetime-local" name="<?= $field ?>" value="<?= isset($values->$field) ? $values->get($field) : '' ?>"></p>
		<?php endif ?>
		<?php if ($type == 'hidden'): ?>
			<input type="hidden" name="<?= $field ?>" value="<?= isset($values->$field) ? $values->$field : '' ?>">
		<?php endif ?>
		<?php if ($type == 'bool'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<div class="radio">
				<label><input type="radio" name="<?= $field ?>" value="1"
					<?= (! isset($values->$field) || ((isset($values->$field) && $values->$field))) ? 'checked' : '' ?>				
					 >Да</label>
				<label><input type="radio" name="<?= $field ?>" value="0"
					 <?= (isset($values->$field) && !$values->$field) ? 'checked' : '' ?>>Нет</label>
			</div>
		<?php endif ?>
		<?php if ($type == 'image'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<img src="<?= isset($values->$field) ? '/upload_images/' . $values->$field : '' ?>" alt="" style="max-width: 50%; max-height: 50%;">
			<p><input type="file" name="<?= $field ?>"></p>
		<?php endif ?>
		<?php if ($type == 'file'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><?= isset($values->$field) ? $values->get($field) : '' ?> </p>
			<p><input type="file" name="<?= $field ?>"></p>
		<?php endif ?>		
		<?php if ($type == 'select'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<?php foreach ($select_variants[$field] as $key => $value): ?>
					<div class="radio">
					<label><input type="radio" name="<?= $field ?>"
					 <?= (isset($values->$field) && $key == $values->$field) ? 'checked' : '' ?> 
					 value="<?= $key ?>">
						<?= $value ?>
					</label>
				</div>
			<?php endforeach ?>
		<?php endif ?>
		<?php if ($type == 'byte'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><input type="number" class="form-control" name="<?= $field ?>" value="<?= isset($values->$field) ? $values->get($field) : '' ?>"></p>
		<?php endif ?>
		<?php if ($type == 'textarea'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><textarea name="<?= $field ?>" id="<?= $field ?>" class="ckeditarea" cols="100" rows="10"><?= isset($values->$field) ? $values->$field : '' ?></textarea></p>
		<?php endif ?>
		<?php if ($type == 'text'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<p><textarea name="<?= $field ?>" id="<?= $field ?>" cols="100" rows="10"><?= isset($values->$field) ? $values->$field : '' ?></textarea></p>
		<?php endif ?>
		<?php if ($type == 'go_to_test_questions'): ?>
			<a href="/my-admin/questions/<?= $values->id ?>" class="btn btn-primary">Вопросы теста</a>
		<?php endif ?>
		<?php if ($type == 'hidden_test_id'): ?>
			<input type="hidden" value="<?= $test_id ?>" name="test_id">
		<?php endif ?>
		<?php if ($type == 'show_test' && isset($values->test_id) && $values->test_id): ?>
			<?php $test = Tests::selectOne('id = '.$values->test_id); ?>
			<p><?= $test->title ?></p>
		<?php endif ?>
		<?php if ($type == 'sting_table'): ?>
			<p><?= $rus_fields_name[$field] ?></p>
			<div class="row">
				<?php for ($i = 0; $i < $max_rows_count; $i++): ?>
					<div class="form-group">
						<div class="col-xs-6">
							<input type="text" name="<?= $field ?>[<?= $i ?>][]" class="form-control" value="<?= isset($values->$field) ? $values->get($field)[$i][0] : '' ?>">
						</div>			
						<div class="col-xs-6">
							<input type="text" name="<?= $field ?>[<?= $i ?>][]" class="form-control" value="<?= isset($values->$field) ? $values->get($field)[$i][1] : '' ?>">
						</div>
					</div>
				<?php endfor; ?>
			</div>
		<?php endif ?>
	<?php endforeach ?>
	<input type="submit" class="btn btn-default dropdown-toggle" value="Сохранить">
</form>
