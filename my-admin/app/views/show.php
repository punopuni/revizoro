<table class="tftable" border="1">
	<?php if (isset($rows) && $rows): ?>
		<tr style="background-color: #BBBBBB ">
			<?php foreach (array_values($rows)[0] as $key => $row): ?>
				<?php if (! isset($visible_fields[$key])) continue ?>
				<?php if ($key == 'id'): ?>
					<td>ID</td>
				<?php else: ?>
					<td><?= $rus_fields_name[$key] ?></td>
				<?php endif ?>
			<?php endforeach ?>
			<?php if (isset($rus_fields_name['go_to_test_questions'])): ?>
				<td>Перейти к вопросам теста</td>
			<?php endif ?>
			<td>Изменить</td>
			<td>Удалить</td>
		</tr>
		<?php foreach ($rows as $row): ?>
			<tr>
				<?php foreach ($row as $field => $value): ?>
					<?php if (! isset($visible_fields[$field])) continue ?>
					<td>
						<?= $row->get($field) ?>
					</td>
				<?php endforeach ?>
				<?php if (isset($rus_fields_name['go_to_test_questions'])): ?>
					<td><a href="/my-admin/questions/test_questions/<?= $row->id ?>" class="btn btn-primary">Вопросы теста</a></td>
				<?php endif ?>
				<td> <a class="btn large-btn btn-success text-right" href="/my-admin/<?= $_GET['controller'] ?>/change/<?= $row->id ?>">Изменить</a></td>
				<td> <a class="btn large-btn btn-danger text-right" href="/my-admin/<?= $_GET['controller'] ?>/delete/<?= $row->id ?>">Удалить</a></td>
			</tr>
		<?php endforeach ?>
	<?php else: ?>
		Записей не обнаружено
	<?php endif ?>
</table>
<?php if (isset($special_id) && $special_id): ?>
	<a href="/my-admin/<?= $_GET['controller'] ?>/create/?create_for=<?= $special_id ?>"><button class="btn btn-primary">Добавить</button></a>
<?php else: ?>
	<a href="/my-admin/<?= $_GET['controller'] ?>/create"><button class="btn btn-primary">Добавить</button></a>
<?php endif ?>
<?php if (isset($fields_for_import)): ?>
	<?= include_file('helpers/file_uploader') ?>
<?php endif ?>