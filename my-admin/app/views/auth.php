<head>
	<link rel="stylesheet" href="/my-admin/static/build/vendor/bootstrap.min.css">
	<link rel="stylesheet" href="/my-admin/static/build/css/style.css">
	<link rel="stylesheet" href="/my-admin/static/build/vendor/font-awesome.min.css">
	<script src="/static/build/vendor/jquery.min.js"></script>
	<script src="/static/build/vendor/ckeditor/ckeditor.js"></script>
	<script src="/static/build/vendor/ckeditor/adapters/jquery.js"></script>
	<script src="/my-admin/static/build/vendor/bootstrap.min.js"></script>
	<script src="/my-admin/static/build/js/all.min.js"></script>
	<meta charset="UTF-8">
</head>
<div class="enter">
	<form method="POST" action="/my-admin/auth">
		<img src="/my-admin/static/build/img/logo.png" alt="">
		<div class="form-group login">
			<label for="login" class="l-text text-left">Логин</label>
			<div class="l-input">
				<input type="text" class="form-control" name="login" placeholder="" value="">
			</div>
		</div>
		<div class="form-group password">
			<label for="password" class="p-text text-left">Пароль</label>
			<div class="p-input">
				<input type="password" class="form-control" name="password" placeholder="" value="">
			</div>
		</div>
		<?php if (isset($auth_error) && $auth_error == 2): ?>
			<p>Заполните поля Логин и Пароль</p>
		<?php endif ?>
		<?php if (isset($auth_error) && $auth_error == 1): ?>
			<p>Не верный Логин или Пароль</p>
		<?php endif ?>
		<input type="Submit" class="btn large-btn btn-default" value="Войти">
	</form>
</div>

