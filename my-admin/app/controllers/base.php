<?php 
/**
* 
*/
class BaseController
{

	
	public static $model = '';

	/**
	 * Генерация данных для показа в таблице
	 */
	public function __construct()
	{
		if (!isset($_SESSION['auth']) || !$_SESSION['auth']) {
			header('Location: /my-admin/auth/');
		}
	}

	public function getTableData()
	{
		$sql_select = '';
		
		$visible_fields = isset($this->visible_fields) && $this->visible_fields ? $this->visible_fields : array();

		foreach ($visible_fields as $field => $type) {
			$sql_select .= '\'' . $field . ',\' ';
		}

		$model = static::$model;

		$visible_data = $model::select('1 = 1', 1000, 'id');

		return $visible_data;

	}

	/**
	 * Отправляем данные в базовый шаблон
	 */
	public function include_to_template($template, $vars = array(), $base_template = 'template')
	{
		$content = include_file($template, $vars);

		// Еcли не объявлен title, берем текущий url
		$title = $_SERVER['REQUEST_URI'];

		// Дополнительные скрипты и стили для каждой страницы
		$extra_scripts = array();
		$extra_styles  = array();

		if (isset($this->title)) {
			$title = $this->title;
		}

		if (isset($this->extra_scripts)) {
			$extra_scripts = $this->extra_scripts;
		}

		if (isset($this->extra_styles)) {
			$extra_styles = $this->extra_styles;
		}

		$vars = array(
			'content' => $content,
			'title' => $title,
			'extra_scripts' => $extra_scripts,
			'extra_styles' => $extra_styles,
		);


		if ($base_template) {
			return include_file($base_template, $vars);
		} else {
			return $content;
		}
	}

	/**
	 * общие Call'ы 
	 */
	public function createCall()
	{	
		$fields = $this->fields;
		$test_id = false;

		if (isset($_GET['create_for']) && $_GET['create_for']) {
			$test_id = $_GET['create_for'];
			}

		$vars = array(
			'fields'  		 	=> $fields,
			'test_id'			=> $test_id,
			'rus_fields_name'	=> $this->rus_fields_name,
			'title'				=> 'Создание записи',
		);
 
		return $this->include_to_template('change', $vars);
	}

	public function indexCall()
	{	
		$rows = $this->getTableData();
		$vars = array(
			'rows'   			=> $rows,
			'rus_fields_name'	=> $this->rus_fields_name,
			'title'				=> 'Список',
			'visible_fields'	=> $this->visible_fields,
		);
		return $this->include_to_template('show', $vars);
	}

	public function changeCall($id)
	{
		$model = new static::$model;
		$values = $model::selectOne($id);

		$vars = array(
			'fields' 			=> $this->fields,
			'title'				=> 'Изменение записи',
			'values' 			=> $values,
			'rus_fields_name'	=> $this->rus_fields_name,
		);
 
		return $this->include_to_template('change', $vars);

	}


	public function saveCall()
	{
		$entity = new static::$model;

		$entity->setAll($_POST);
		$test_id = '';

		if (isset($entity->test_id) && $entity->test_id) {
			$test_id = '/test_questions/'.$entity->test_id;
		}

		$entity->save();

		header('Location: /my-admin/' . $_GET['controller'] . $test_id);
	}

	public function updateCall($id)
	{
		
		$entity = new static::$model;

		$entity = $entity->load($id);
		
		$entity->setAll($_POST);

		$entity->update();

		header('Location: /my-admin/' . $_GET['controller']);

	}

	public function deleteCall($id)
	{
		$model = new static::$model;
		$model->drop($id);
		header('Location: /my-admin/' . $_GET['controller']);
	}
}