<?php 
/**
* 
*/
class ConfigController extends BaseController
{
	public $title = 'Настройки';

	public $max_rows_count = 6;

	public $configs = array(
		'admin_mails'			=> 'string',
		'license' 				=> 'string',
		'footer_address'  		=> 'textarea',
		'meta_title'			=> 'string',
		'meta_description'		=> 'text',
		'meta_keywords'			=> 'text',
	);

	public $rus_fields_name = array(
		'admin_mails'			=> 'Email(ы) для отправки сообщений',
		'license'	 			=> 'Лицензия на главной',
		'footer_address' 		=> 'Адрес в футере',
		'meta_title'			=> 'Meta title',
		'meta_description'		=> 'Meta description',
		'meta_keywords'			=> 'Meta keywords',
	);

	public function indexCall()	
	{
		return $this->generateConfigs();
	}	


	public function generateConfigs()
	{

		$myfile = $this->configFile();
		$fields = $this->configs;
		if (! file_exists($myfile)) {
			file_put_contents($myfile, '');
		}
		$data = file_get_contents($myfile);
		$data = json_decode($data);

		$values = new BaseModel;
		foreach ($data as $key => $value) {
			$values->$key = $value;
		}

		$vars = array(
			'values'            => $values,
			'fields'            => $fields,
			'rus_fields_name'   => $this->rus_fields_name,
			'max_rows_count'	=> $this->max_rows_count,
		);

		return $this->include_to_template('change', $vars);
	}

	public function saveCall()
	{
		$myfile = $this->configFile();
		$fields = $this->configs;
		$data = array();

		foreach ($fields as $field => $type) {
			if (isset($_POST[$field]) && $_POST[$field]) {
				$data[$field] = $_POST[$field];
			}
		}

		$data = json_encode($data);
		file_put_contents($myfile, $data);
		header('Location: /my-admin/' . $_GET['controller']);
	}

	public function configFile()
	{
		return $_SERVER['DOCUMENT_ROOT'] . "/my-admin/configs/config.cfg";
	}
	
}