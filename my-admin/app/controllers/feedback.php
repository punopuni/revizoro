<?php 
/**
* 
*/
class FeedbackController extends BaseController
{
	public static $model = 'Feedback';
	public static $edit_only = true;
	public $title = 'Обратная связь';
	
	public $fields = array(
		'id'		  	  		=> 'false',
		'name'	 				=> 'string',
		'email'					=> 'string',
		'phone'					=> 'string',
		'message'				=> 'textarea',
	);

	public $visible_fields = array(
		'id'		  	  		=> 'false',
		'name'	 				=> 'string',
		'email'					=> 'string',
		'phone'					=> 'string',
	);

	public $rus_fields_name = array(
		'name'	 				=> 'Имя',
		'email'					=> 'Email',
		'phone'					=> 'Телефон',
		'message'				=> 'Сообщение',
	);

}
