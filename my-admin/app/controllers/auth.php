<?php 
/**
* Страница авторизации
*/
class authController extends BaseController
{
	public $title = 'Авторизация';
	public static $model = 'User';

	public function __construct(){
	}

	public function indexCall()
	{	

		if (isset($_SESSION['auth']) && $_SESSION['auth']) {
			header('Location: /my-admin/');
		}

		$login = false;
		$password = false;
		$user = array();
		$vars = array();
		$auth_error = 0;

		if (! isset($_POST['login']) || ! isset($_POST['password'])) {
			$vars = array(
				'auth_error' => $auth_error,
			);
			return include_file('auth', $vars);
		}

		if ($_POST['login'] && $_POST['password']) {
			$login = $_POST['login'];
			$password = $_POST['password'];
		} else {
			$auth_error = 2;
			$vars = array(
				'auth_error' => $auth_error,
			);
			return include_file('auth', $vars);
		}

		$model = static::$model;
		$user = $model::select('login = \'' . $login . '\' AND password = \'' . $password . '\'');

		if ($user) {
			$_SESSION['auth'] = $user;
			return header('Location: /my-admin/');
		} else {
			$auth_error = 1;
		}

		$vars = array(
			'auth_error' => $auth_error,
		);

		return include_file('auth', $vars);
	}

	public function logoutCall()
	{
		if (isset($_POST['logout'])) {
			$_SESSION['auth'] = '';
			return header('Location: /my-admin/auth');
		}
		return header('Location: /my-admin/');
	}

}