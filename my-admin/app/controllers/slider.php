<?php
/**
*
*/
class SliderController extends BaseController
{
	public static $model = 'Slider';
	public $title = 'Слайдер';

	public $fields = array(
		'id'		  	  		=> 'false',
		'active' 				=> 'bool',
		'idx'					=> 'integer',
		'image'					=> 'image',

	);

	public $visible_fields = array(
		'idx'              		=> 'integer',
	);

	public $rus_fields_name = array(
		'active' 				=> 'Активно',
		'idx'					=> 'Порядок',
		'image'					=> 'Изображение',
	);
}