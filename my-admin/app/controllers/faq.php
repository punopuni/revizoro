<?php
/**
*
*/
class FaqController extends BaseController
{
	public static $model = 'Faq';
	public $title = 'Вопрос-Ответ';

	public $fields = array(
		'id'		  	  		=> 'false',
		'active' 				  => 'bool',
		'question'				=> 'text',
		'answer'					=> 'text',

	);

		public $visible_fields = array(
		'active' 				=> 'Активно',
		'question'			=> 'Вопрос',
		'answer'			  => 'Ответ',
	);

	public $rus_fields_name = array(
		'active' 				=> 'Активно',
		'question'			=> 'Вопрос',
		'answer'			  => 'Ответ',
	);
}