<?php
/**
*
*/
class ReviewController extends BaseController
{
	public static $model = 'Review';
	public $title = 'Наши клиенты';

	public $fields = array(
		'id'		  	  		=> 'false',
		'active' 				  => 'bool',
		'image'				=> 'image',
		'description'					=> 'text',

	);

		public $visible_fields = array(
		'active' 			=> 'Активно',
//		'image'			    => 'Изображение',
		'description'			    => 'Описание',
	);

	public $rus_fields_name = array(
		'active' 			=> 'Активно',
		'image'			    => 'Изображение',
		'description'			    => 'Описание',
	);
}