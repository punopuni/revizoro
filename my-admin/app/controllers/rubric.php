<?php 
/**
* 
*/
class RubricController extends BaseController
{
	public static $model = 'rubric';
	public $title = 'Страницы сайта';
	
	public $fields = array(
		'id'		  	  => 'false',
		'title' 		  => 'string',
		'active'		  => 'bool',
		'text'			  => 'textarea',
		'furl'			  => 'hidden',
	);

	public $visible_fields = array(
		'id'   		  		=> 'integer',
		'title'		  		=> 'string',
	);

	public $rus_fields_name = array(
		'title' 		  => 'Заголовок',
		'active' 		  => 'Активно',
		'text'		  	  => 'Текст',
	);

}