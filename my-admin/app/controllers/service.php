<?php 
/**
* 
*/
class ServiceController extends BaseController
{
	public static $model = 'Service';
	public $title = 'Сервисы на главной';
	
	public $fields = array(
		'id'		  	  		=> 'false',
		'active' 				=> 'bool',
		'description'			=> 'string',
		'icon'					=> 'image',

	);

	public $visible_fields = array(
		'id'              		=> 'integer',
		'description' 			=> 'string',
	);

	public $rus_fields_name = array(
		'active' 				=> 'Активно',
		'description'			=> 'Описание',
		'icon'					=> 'Иконка',
	);

}
