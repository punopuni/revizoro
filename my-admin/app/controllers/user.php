<?php 
/**
* 
*/
class UserController extends BaseController
{
	public static $model = 'User';
	public $title = 'Администраторы сайта';
	
	public $fields = array(
		'id' 				=> 'false',
		'active' 			=> 'bool',
		'login'				=> 'string',
		'password'			=> 'password',
	);

	public $visible_fields = array(
		'id' 				=> 'integer',
		'login'				=> 'string',
	);

	public $rus_fields_name = array(
		'active' 			=> 'Активно',
		'login'				=> 'Логин',
		'password'			=> 'Пароль',
	);


}