var gulp       = require('gulp'),
    jade       = require('gulp-jade'),
    concat     = require('gulp-concat'),
    uglify     = require('gulp-uglify'),
    imagemin   = require('gulp-imagemin'),
    sourcemaps = require('gulp-sourcemaps'),
    stylus     = require('gulp-stylus'),
    del        = require('del'),
    gutil      = require('gulp-util'),
    plumber    = require('gulp-plumber');
    minifycss  = require('gulp-minify-css');

var build_paths = {
  scripts: 'static/assets/js/**/*.js',
  images: 'static/assets/img/**/*',
  jade: 'static/assets/jade/**/[^_]*.jade',
  stylus: 'static/assets/stylus/**/[^_]*.styl',
  font_awesome: 'static/assets/vendor/font-awesome/*.scss',
  fonts: 'static/assets/fonts/**/*',
  fonts: 'static/assets/fonts/**/*',
  vendor: 'static/assets/vendor/**/*'
};

var watch_paths = {
  scripts: 'static/assets/js/**/*.js',
  images: 'static/assets/img/**/*',
  jade: 'static/assets/jade/**/*.jade',
  stylus: 'static/assets/stylus/**/*.styl',
  fonts: 'static/assets/fonts/**/*',
  vendor: 'static/assets/vendor/**/*'
};

var dest = 'static/build/';

gulp.task('clean', function(cb) {
  del(['build/*'], cb);
});

// Copy all static images
gulp.task('vendor', function() {
  return gulp.src(build_paths.vendor)
    .pipe(gulp.dest(dest + 'vendor'));
});

gulp.task('scripts', function() {
  return gulp.src(build_paths.scripts)
    .pipe(uglify())
    .pipe(sourcemaps.init())
    .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dest + 'js'));
});

// Copy all static images
gulp.task('images', function() {
  return gulp.src(build_paths.images)
    // Pass in options to the task
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest(dest + 'img'));
});

// Copy all static images
gulp.task('fonts', function() {
  return gulp.src(build_paths.fonts)
    .pipe(gulp.dest(dest + 'fonts'));
});

gulp.task('jade', function() {
    return gulp.src(build_paths.jade)
        .pipe(jade({
            pretty: true
        }))
        .on('error', gutil.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest(dest)) // Записываем собранные файлы
    ;
});

gulp.task('font_awesome', function () {
  return gulp.src(build_paths.font_awesome)
    // .pipe(sourcemaps.init())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(dest + 'css')) // записываем css
    ;
});

gulp.task('stylus', function() {
    return gulp.src(build_paths.stylus)
      .pipe(plumber()) // собираем stylus
    .pipe(sourcemaps.init())
    .pipe(stylus())
    .pipe(sourcemaps.write())
    .pipe(minifycss())
    .pipe(gulp.dest(dest + 'css')) // записываем css
    ;
});
gulp.task('watch', function() {
  gulp.watch(watch_paths.scripts, ['scripts']);
  gulp.watch(watch_paths.images, ['images']);
  gulp.watch(watch_paths.jade, ['jade']);
  gulp.watch(watch_paths.fonts, ['fonts']);
  gulp.watch(watch_paths.stylus, ['stylus']);
  gulp.watch(watch_paths.vendor, ['vendor']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', [
  'watch',
  'font_awesome',
  'scripts',
  'images',
  'jade',
  'stylus',
  'fonts',
  'vendor'
]);