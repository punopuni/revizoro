<?php 

define("SITE_WORK_DIR", $_SERVER['DOCUMENT_ROOT'].'/app/controllers/', true);
define("SITE_SHOW_DIR", $_SERVER['DOCUMENT_ROOT'].'/app/views/', true);
define("PREFIX", 'ru_', true);

date_default_timezone_set('Asia/Almaty');

if (getenv('ENV') == 'docker_dev') {
	error_reporting(E_ALL);
} else {
	error_reporting(0);
}

require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
spl_autoload_register(function ($class) {
	if (strpos($class, 'Controller') !== FALSE) {
		$class = str_replace('Controller', '', $class);
		$class = strtolower($class);

		$file = $_SERVER['DOCUMENT_ROOT']. '/app/controllers/' . $class . '.php';
		if (file_exists($file)) {
			require_once $file;
		}
	} elseif (file_exists($_SERVER['DOCUMENT_ROOT'].'/app/models/'.strtolower($class).'.php')) {
		require_once $_SERVER['DOCUMENT_ROOT'].'/app/models/' . strtolower($class) . '.php';
	}
});

BaseModel::__connect();

$controller = isset($_GET['controller']) && $_GET['controller'] ? $_GET['controller'] : 'main';
$method = isset($_GET['method']) && $_GET['method'] ? $_GET['method'] : 'index';
$id     = isset($_GET['id']) && $_GET['id'] ? $_GET['id'] : '';

session_start();

$cfg_file = $_SERVER['DOCUMENT_ROOT'] . "/my-admin/configs/config.cfg";

if (file_exists($cfg_file)) {
	$config_data = file_get_contents($cfg_file);
	$GLOBALS['config'] = (array) json_decode($config_data);
}

if (! isset($_SESSION['lang'])) {
	$_SESSION['lang'] = 'ru';
}

$file = $_SERVER['DOCUMENT_ROOT'] .'/app/controllers/'. $controller . '.php';

if (file_exists($file)) {
	require_once $file;
} else {
	do404();
}
	

function _lang($word)
{
	$lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'ru';

	require $_SERVER['DOCUMENT_ROOT']. '/languages/' . $lang . '.php';

	return isset($lang_words[$word]) ? $lang_words[$word] : $word;
}

function include_file($file, $vars = array())
{
	
	foreach ($vars as $name => $var) {
		$$name = $var;
	}

	ob_start();

	$file = SITE_SHOW_DIR . $file . '.php';

	if (file_exists($file)) {
		require $file;
	}

	$return = ob_get_clean();

	return $return;

}

function do404()
{
    header('HTTP/1.1 404 Not Found');
    exit;
}

$controller = $controller . 'Controller';
$method = $method . 'Call';
if (! method_exists($controller, $method)) die('502');

$controller = new $controller;

echo $controller->{$method}($id);