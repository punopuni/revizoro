<?php 
$lang_words = array(
	'add_word' => 'Добавить слово',
	'answer_btn' => 'Ответить',
	'restart_btn' => 'Рестарт',
	'select_lang' => 'Выбрать язык',
	'change_lang_btn' => 'Выбрать',
	'uncorrect_word' => 'Неправильный ввод слова',
	'uncorrect_first_letter' => 'Первая буква слова не совпадает с последней буквой предыдущего слова',
	'word_doesnt_exists' => 'нет такого слова',
	'called_word' => 'Это слово уже произносилось',
	'user_win' => 'Вы выиграли',
);
