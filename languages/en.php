<?php 
$lang_words = array(
	'add_word' => 'Enter your word',
	'answer_btn' => 'Answer',
	'restart_btn' => 'Restart',
	'select_lang' => 'Select language',
	'change_lang_btn' => 'Choose',
	'uncorrect_word' => 'Uncorrect word',
	'uncorrect_first_letter' => 'First letter of imposed word is differens from first letter of last word',
	'word_doesnt_exists' => 'Sorry =( I can\'t find this word in my base.',
	'called_word' => 'This word previously called',
	'user_win' => 'Ok, now you defeated me, and what now?',
);
